﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Persona
    {
        // Solo lectura
        private int dni;
        public int Dni {
            get { return dni; }
        }

        // Solo escritura
        private int numeroFavorito;
        public int NumeroFavorito {
            set { numeroFavorito = value; }
        }

        // Escritura-lectura
        private string alias;
        public string Alias {
            get { return alias; }
            set { alias = value; }
        }

        // Predeterminada
        public int Codigo {
            get { return 52; }
        }

        public Persona(int dni)
        {
            this.dni = dni;
        }
    }
}
