﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    class Contador
    {
        public int Total { get; set; }

        public Contador()
        {
            Total = 0;
        }

        public Contador(int inicio)
        {
            Total = inicio;
        }

        public void Sumar(int num)
        {
            Total += num;
        }

        public void Sumar(int num1, int num2)
        {
            Total += num1 + num2;
        }

        public void Sumar(int num, bool siEsMenorQue100)
        {
            if (!siEsMenorQue100 || Total < 100) Total += num;
        }
    }
}
