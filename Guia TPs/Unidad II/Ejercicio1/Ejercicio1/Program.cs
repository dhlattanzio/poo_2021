﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    /*
     * Desarrollar un programa que posea una estructura de clases donde se puedan
     * observar dos métodos y el constructor  sobrecargados.
     */
    class Program
    {
        static void Main(string[] args)
        {
            Contador contador = new Contador(50);
            Console.WriteLine($"Contador = {contador.Total}");
            contador.Sumar(10, 20);
            Console.WriteLine($"Contador = {contador.Total}");
            contador.Sumar(100, true);
            Console.WriteLine($"Contador = {contador.Total}");
            contador.Sumar(500, true);
            Console.WriteLine($"Contador = {contador.Total}");
            contador.Sumar(5, false);
            Console.WriteLine($"Contador = {contador.Total}");
            contador.Sumar(115);
            Console.WriteLine($"Contador = {contador.Total}");
            Console.ReadLine();
        }
    }
}
