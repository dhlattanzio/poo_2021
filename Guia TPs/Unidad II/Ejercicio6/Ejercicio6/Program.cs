﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio6
{
    /*
     * Desarrollar un programa donde se observe claramente el uso de los miembros
     * de base en un entorno de herencia.  Enfatice las particularidades al usarlo en
     * los constructores y finalizadores. Demuestre en el mismo programa el uso de this.
     * Establezca las diferencias y en qué caso se justifica utilizar cada uno.
     */
    class Program
    {
        static void Main(string[] args)
        {
            Auto auto = new Auto(100);
            Console.WriteLine($"Velocidad: {auto.Velocidad}");
            Console.WriteLine($"Velocidad 2: {auto.VelocidadPadre()}");
            Console.ReadLine();
        }
    }
}
