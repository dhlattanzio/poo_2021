﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio6
{
    class Auto : Vehiculo
    {
        public int Velocidad { get; set; }

        public Auto(int velocidad)
        {
            this.Velocidad = velocidad;
            base.Velocidad = this.Velocidad / 2;
        }

        ~Auto()
        {
            base.Historial.Clear();
            base.Historial = null;
        }

        public int VelocidadPadre()
        {
            return base.Velocidad;
        }
    }
}
