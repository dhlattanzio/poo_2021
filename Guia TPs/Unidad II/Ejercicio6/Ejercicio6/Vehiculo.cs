﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio6
{
    class Vehiculo
    {
        public int Velocidad { get; set; }
        public List<string> Historial { get; set; } = new List<string>();
    }
}
