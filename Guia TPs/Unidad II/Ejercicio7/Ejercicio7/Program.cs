﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio7
{
    /*
     * Desarrollar un programa que posea al menos una clase abstracta, un método virtual,
     * una clase sellada y una clase anidada.
     */
    class Program
    {
        static void Main(string[] args)
        {
            NotebookRota notebook = new NotebookRota();
            notebook.Encender();
            notebook.UsarPad();

            Calculadora calculadora = new Calculadora();
            calculadora.Sumar(10, 40);
        }
    }
}
