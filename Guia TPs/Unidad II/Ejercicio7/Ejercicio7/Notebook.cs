﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio7
{
    class Notebook : Computadora
    {
        public override void Encender()
        {
            Console.WriteLine("Encendida");
        }

        public virtual void UsarPad()
        {
            Console.WriteLine("Pad usado");
        }
    }
}
