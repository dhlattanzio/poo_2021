﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio7
{
    sealed class Calculadora : Computadora
    {
        private OperacionSuma operacionSuma = new OperacionSuma();

        public override void Encender()
        {
            Console.WriteLine("Encendida");
        }

        public int Sumar(int n1, int n2)
        {
            return operacionSuma.Operar(n1, n2);
        }

        class OperacionSuma
        {
            public int Operar(int n1, int n2)
            {
                return n1 + n2;
            }
        }
    }
}
