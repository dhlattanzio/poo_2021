﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio3
{
    /*
     * Desarrollar un programa que posea una clase que aplique un destructor que
     * finalice el ciclo de vida de un onjeto que se instranció en el constructor.
     */
    class Program
    {
        static void Main(string[] args)
        {
            Maquina maquina = new Maquina();
            maquina = null;
        }
    }
}
