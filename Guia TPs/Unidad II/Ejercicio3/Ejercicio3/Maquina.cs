﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio3
{
    class Maquina
    {
        private List<int> numeros;

        public Maquina()
        {
            numeros = new List<int>();
            for(int i=0;i<1000;i++)
            {
                numeros.Add(i);
            }
        }

        ~Maquina()
        {
            numeros.Clear();
            numeros = null;
        }
    }
}
