﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio4
{
    class Correo
    {
        public List<string> MensajesRecibidos { get; set; } = new List<string>();
        public List<string> MensajesEnviados { get; set; } = new List<string>();

        public event EventHandler EventoMensajeEnviado;
        public event EventHandler EventoMensajesBorrados;

        public Correo()
        {
            EventoMensajeEnviado += NotificarMensajeEnviado;
            EventoMensajesBorrados += NotificarMensajesEliminados;
        }

        public void EnviarMensaje(string mensaje)
        {
            MensajesEnviados.Add(mensaje);
            EventoMensajeEnviado.Invoke(this, new EventArgs());
        }

        public void BorrarMensajesRecibidos()
        {
            MensajesRecibidos.Clear();
            EventoMensajesBorrados.Invoke(this, new EventArgs());
        }

        private void NotificarMensajeEnviado(Object obj, EventArgs e)
        {
            Console.WriteLine("Mensaje enviado!");
        }

        private void NotificarMensajesEliminados(Object obj, EventArgs e)
        {
            Console.WriteLine("Mensajes eliminados!");
        }
    }
}
