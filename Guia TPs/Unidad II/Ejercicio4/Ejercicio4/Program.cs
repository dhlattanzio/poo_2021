﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio4
{
    /*
     * Desarrollar un programa que posea una clase que tenga propiedades, métodos y sucesos.
     * Los sucesos deben tener suscripciones manuales realizadas por el programador a funciones
     * de la clase donde se en cuentra instanciado el objeto que posee los sucesos.
     */
    class Program
    {
        static void Main(string[] args)
        {
            Correo correo = new Correo();
            correo.EnviarMensaje("Hola, Mundo!");
            correo.BorrarMensajesRecibidos();
            Console.ReadLine();
        }
    }
}
