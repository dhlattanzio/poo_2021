﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio5
{
    class Impresora
    {
        private int hojas = 0;
        private int totalImpresiones = 0;

        public string Marca { get; set; }
        public bool Encendida { get; set; } = false;

        public static event EventHandler eventoEncender;
        public static event EventHandler eventoImprimir;

        public Impresora(string marca)
        {
            Marca = marca;
            eventoEncender += EventoImpresoraEncender;
            eventoImprimir += EventoImpresoraImprimir;
        }

        public void CargarHojas(int hojas)
        {
            this.hojas += hojas;
        }
        
        public void Imprimir(string texto)
        {
            if (Encendida && hojas > 0)
            {
                hojas--;
                totalImpresiones++;
                eventoImprimir.Invoke(this, null);
            }
        }

        public void Encender()
        {
            if (!Encendida)
            {
                Encendida = true;
                eventoEncender.Invoke(this, null);
            }
        }

        public void Apagar()
        {
            if (Encendida) Encendida = false;
        }

        public static void EventoImpresoraEncender(Object obj, EventArgs e)
        {
            Console.WriteLine("Evento: Impresora encendida!");
        }

        public static void EventoImpresoraImprimir(Object obj, EventArgs e)
        {
            Console.WriteLine("Evento: Texto impreso!");
        }
    }
}
