﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio5
{
    /*
     * Desarrollar un programa que posea una clase que contenga al menos dos campos,
     * tres métodos, un constructor y dos sucesos estáticos. Comente que característica
     * le otorga esta característica a cada miembro.
     * 
     * Al ser los sucesos estaticos, estos guardar una referencia a todos los objetos que se subscriban.
     * Cuando el objeto activa el evento, se les avisa a todos.
     * Cada vez que se crea una nueva instancia, se agregan mas suscriptores.
     */
    class Program
    {
        static void Main(string[] args)
        {
            Impresora impresora1 = new Impresora("HP");
            Impresora impresora2 = new Impresora("EPSON");

            impresora1.CargarHojas(100);
            impresora1.Encender();
            impresora2.CargarHojas(100);
            impresora2.Encender();

            impresora2.Imprimir("Hola!");

            Console.ReadLine();
        }
    }
}
