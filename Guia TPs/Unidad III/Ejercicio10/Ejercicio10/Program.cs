﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio10
{
    /*
     * Desarrollar un programa que aplique el concepto de manejo de errores.
     * La estructura propuesta debe tener al menos 5 Catch y el finally.
     */
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // int.Parse("abc");
                int.Parse("9999999999999");
                //"hola".IndexOf('2', 99999);
            }
            catch (FormatException e)
            {
                Console.WriteLine($"Formato de numero incorrecto: {e.Message}");
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine($"Indice fuera de los limites: {e.Message}");
            }
            catch (OverflowException e)
            {
                Console.WriteLine($"Overflow: {e.Message}");
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine($"No se puede dividir por cero: {e.Message}");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception generica: {e.Message}");
            } finally
            {
                Console.WriteLine("Finally");
            }

            Console.ReadLine();
        }
    }
}
