﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio11
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Computadora computadora = new Computadora();
                computadora.Usar();
                computadora.Rota = true;
                computadora.Usar();
            } catch (ComputadoraRotaException e)
            {
                Console.WriteLine($"Exception: {e.Message}");
            }

            Console.ReadLine();
        }
    }
}
