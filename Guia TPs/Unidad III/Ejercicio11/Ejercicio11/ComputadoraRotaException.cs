﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio11
{
    class ComputadoraRotaException : Exception
    {
        public ComputadoraRotaException() : base("La computadora esta rota")
        {
        }
    }
}
