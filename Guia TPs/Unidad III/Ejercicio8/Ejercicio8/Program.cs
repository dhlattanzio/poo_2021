﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio8
{
    /*
     * Desarrollar un programa que genera varias instancias (una cantidad importante), 
     * verifique la memoria utilizada, pase el GC y vuelva a verificar el espacio de memoria.
     * ¿Qué se observa?
     * 
     * Se puede observar que, antes de que pase el GC, el programa ocupa una gran cantidad de memoria.
     * Una vez llamado al GC, este libera todos los recursos sin referencias de la memoria.
     */
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"GC: {GC.GetTotalMemory(false)}");
            List<long> tmp = new List<long>();
            for(int i=0;i<1000000;i++)
            {
                tmp.Add(i);
            }
            Console.WriteLine($"GC: {GC.GetTotalMemory(false)}");

            tmp = null;
            GC.Collect();
            Console.WriteLine($"GC: {GC.GetTotalMemory(false)}");

            Console.ReadLine();
        }
    }
}
