﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio9
{
    /*
     * Desarrollar un programa que genere una instancia, pierda la referencia a la misma
     * y aplicando la técnica de “resurrección de objetos” logre obtener la referencia
     * a ese mismo objeto.
     */
    class Program
    {
        public static Persona revivir = null;

        static void Main(string[] args)
        {
            Console.WriteLine($"Obj revivir: {revivir}");

            Persona persona = new Persona("Diego");
            Console.WriteLine($"Obj persona: {persona}");

            persona = null;
            GC.Collect();

            Console.WriteLine($"Obj persona: {persona}");
            Console.WriteLine($"Obj revivir: {revivir}");

            persona = revivir;
            revivir = null;
            Console.WriteLine($"Obj persona: {persona}");

            Console.ReadLine();
        }

        public static void Revivir(Persona persona)
        {
            revivir = persona;
        }
    }
}
