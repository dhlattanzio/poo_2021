﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio3
{
    /*
     * Desrrollar un programa que utilice expresiones lambda.
     */
    class Program
    {
        static void Main(string[] args)
        {
            new Program();
        }

        public Program()
        {
            Console.WriteLine($"{5} + {15} = {sumar(5, 15)}");
            Console.WriteLine($"{5} - {10} = {restar(5, 10)}");
            Console.WriteLine($"{5} * {5} = {multiplicar(5, 5)}");
            Console.WriteLine($"{50} / {2} = {dividir(50, 2)}");
            Console.ReadLine();
        }

        public int sumar(int n1, int n2) => n1 + n2;
        public int restar(int n1, int n2) => n1 - n2;
        public int multiplicar(int n1, int n2) => n1 * n2;
        public int dividir(int n1, int n2) => n1 / n2;
    }
}
