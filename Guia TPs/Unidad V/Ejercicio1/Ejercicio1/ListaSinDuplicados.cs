﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    class ListaSinDuplicados<T>
    {
        private List<T> lista = new List<T>();

        public void Agregar(T elemento)
        {
            if (!lista.Contains(elemento)) lista.Add(elemento);
        }

        public bool Eliminar(T elemento)
        {
            return lista.Remove(elemento);
        }

        public List<T> ObtenerLista()
        {
            return lista;
        }
    }
}
