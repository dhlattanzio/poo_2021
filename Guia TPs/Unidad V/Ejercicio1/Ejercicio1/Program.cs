﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    /*
     * Desarrollar un programa que aplique el concepto genéricos a nivel de clase y en almenos dos métodos.
     */
    class Program
    {
        static void Main(string[] args)
        {
            ListaSinDuplicados<int> lista = new ListaSinDuplicados<int>();
            lista.Agregar(23);
            lista.Agregar(3);
            lista.Agregar(6);
            lista.Agregar(87);
            lista.Agregar(12);
            lista.Agregar(6);
            lista.Agregar(3);

            foreach(int num in lista.ObtenerLista())
            {
                Console.WriteLine(num);
            }

            Console.ReadLine();
        }
    }
}
