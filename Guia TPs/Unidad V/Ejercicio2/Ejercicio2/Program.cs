﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    /*
     * Desarrollar un programa que utilice Linq para realizar consultas.
     */
    class Program
    {
        static void Main(string[] args)
        {
            List<int> lista = new List<int>() { 1, 5, 8, 14, 82, 43, 78, 1, 33, 85, 43, 93, 16, 71, 22, 54 };

            Console.WriteLine($"Contiene 85: {lista.Any(x => x == 85)}");
            Console.WriteLine($"Suma total: {lista.Aggregate(0, (acc, num) => acc + num)}");
            Console.WriteLine($"Cantidad menores de 10: {lista.Count(x => x < 10)}");
            Console.ReadLine();
        }
    }
}
