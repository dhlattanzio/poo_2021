﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio15
{
    class ContenedorNombres : IEnumerable, IEnumerator
    {
        private List<string> nombres =new List<string>();

        private int index = -1;
        public object Current => nombres[index];

        public void AgregarNombre(string nombre)
        {
            nombres.Add(nombre);
        }

        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }

        public bool MoveNext()
        {
            if (index < nombres.Count - 1)
            {
                index++;
                return true;
            }
            return false;
        }

        public void Reset()
        {
            index = -1;
        }
    }
}
