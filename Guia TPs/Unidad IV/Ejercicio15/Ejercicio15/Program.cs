﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio15
{
    /*
     * Desarrollar un programa que implemente las interfaces IEnumerable e IEnumerator.
     * Lo adaptado recórralo con el for each y muestre los resultados.
     */
    class Program
    {
        static void Main(string[] args)
        {
            ContenedorNombres contenedor = new ContenedorNombres();
            contenedor.AgregarNombre("Diego");
            contenedor.AgregarNombre("Sofia");
            contenedor.AgregarNombre("Hernan");
            contenedor.AgregarNombre("Maria");

            foreach(string nombre in contenedor)
            {
                Console.WriteLine(nombre);
            }
            Console.ReadLine();
        }
    }
}
