﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio13
{
    class Libro
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Codigo { get; set; }
        public int Paginas { get; set; }
        public int Edicion { get; set; }

        public override string ToString()
        {
            return $"{Id} - {Codigo} - {Edicion} - {Paginas} - {Nombre}";
        }
    }
}
