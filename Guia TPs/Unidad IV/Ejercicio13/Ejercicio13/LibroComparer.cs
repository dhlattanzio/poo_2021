﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio13
{
    class LibroComparer : IComparer<Libro>
    {
        public int Compare(Libro x, Libro y)
        {
            if (x.Id != y.Id) return x.Id - y.Id;
            if (x.Codigo != y.Codigo) return x.Codigo - y.Codigo;
            if (x.Edicion != y.Edicion) return x.Edicion - y.Edicion;
            if (x.Paginas != y.Paginas) return x.Paginas - y.Paginas;
            return x.Nombre.CompareTo(y.Nombre);
        }
    }
}
