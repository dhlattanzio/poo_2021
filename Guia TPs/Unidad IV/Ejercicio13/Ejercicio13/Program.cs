﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio13
{
    /*
     * Desarrollar un programa que implemente la interfaz IComparer.
     * La clase que lo implementa deberá tener al menos cinco criterios de ordenamiento.
     * Genere un arrary de objetos y ordénelos por cada criterio. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            List<Libro> libros = new List<Libro>();

            libros.Add(new Libro() { Id = 1, Codigo = 1, Edicion = 1, Paginas = 300, Nombre = "Harry Potter" });
            libros.Add(new Libro() { Id = 3, Codigo = 6, Edicion = 1, Paginas = 100, Nombre = "Star Wars" });
            libros.Add(new Libro() { Id = 1, Codigo = 1, Edicion = 2, Paginas = 350, Nombre = "Harry Potter" });
            libros.Add(new Libro() { Id = 6, Codigo = 17, Edicion = 7, Paginas = 700, Nombre = "El principito" });

            foreach (Libro libro in libros)
            {
                Console.Write(libro.ToString() + ", ");
            }

            libros.Sort(new LibroComparer());
            Console.WriteLine();
            foreach (Libro libro in libros)
            {
                Console.Write(libro.ToString() + ", ");
            }
            Console.ReadLine();
        }
    }
}
