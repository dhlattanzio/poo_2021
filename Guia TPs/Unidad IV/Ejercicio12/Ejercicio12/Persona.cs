﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio12
{
    class Persona : IComparable
    {
        public int Edad { get; set; }

        public Persona(int edad)
        {
            Edad = edad;
        }

        public int CompareTo(object obj)
        {
            if (obj is Persona == false) return 0;
            return Edad - ((Persona)obj).Edad;
        }
    }
}
