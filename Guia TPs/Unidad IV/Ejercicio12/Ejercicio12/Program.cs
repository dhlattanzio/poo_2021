﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio12
{
    /*
     * Desarrollar un programa que implemente la interfaz IComparable.
     * Genere un arrary de objetos y ordénelos. 
     */
    class Program
    {
        static void Main(string[] args)
        {
            List<Persona> personas = new List<Persona>();

            Random random = new Random();
            for(int i=0;i<10;i++)
            {
                personas.Add(new Persona(random.Next(10, 90)));
            }

            foreach (Persona persona in personas)
            {
                Console.Write($"{persona.Edad}, ");
            }

            Console.WriteLine("Ordenando...");
            personas.Sort();

            foreach(Persona persona in personas)
            {
                Console.Write($"{persona.Edad}, ");
            }

            Console.ReadLine();
        }
    }
}
