﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio14
{
    class ContenedorNumeros : ICloneable
    {
        public List<int> Numeros { get; } = new List<int>();

        public void AgregarNumero(int num)
        {
            Numeros.Add(num);
        }

        public object Clone()
        {
            ContenedorNumeros obj = new ContenedorNumeros();
            obj.Numeros.AddRange(Numeros);
            return obj;
        }
    }
}
