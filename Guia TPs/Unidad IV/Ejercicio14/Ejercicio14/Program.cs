﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio14
{
    /*
     * Desarrollar un programa que implemente la interfaz ICloneable.
     * Clone el objeto que posee la interfaz y demuestre que la clonación funcionó.
     */
    class Program
    {
        static void Main(string[] args)
        {
            ContenedorNumeros contenedor1 = new ContenedorNumeros();
            contenedor1.AgregarNumero(5);
            contenedor1.AgregarNumero(10);
            contenedor1.AgregarNumero(50);

            ContenedorNumeros contenedor2 = (ContenedorNumeros)contenedor1.Clone();

            ImprimirContenedor(contenedor1);
            ImprimirContenedor(contenedor2);

            Console.WriteLine();
            contenedor1.Numeros.RemoveAt(1);
            contenedor1.Numeros.RemoveAt(1);

            ImprimirContenedor(contenedor1);
            ImprimirContenedor(contenedor2);

            Console.ReadLine();
        }

        private static void ImprimirContenedor(ContenedorNumeros contenedor)
        {
            foreach(int num in contenedor.Numeros)
            {
                Console.Write($"{num}, ");
            }
            Console.WriteLine();
        }
    }
}
