﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Servidor
{
    /*
    * Desarrollar un programa que utilizando socket permita escribir en una
    * aplicación que se esté ejecutando en una PC de la red y lo escrito se
    * pueda visualizar en la aplicación que está corriendo en otra PC de la red.
    */
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                TcpListener servidor = new TcpListener(System.Net.IPAddress.Any, 5000);
                servidor.Start();

                Console.WriteLine("Esperando cliente...");
                TcpClient cliente = servidor.AcceptTcpClient();
                Console.WriteLine("Cliente conectado!");

                while (true)
                {
                    string texto = Console.ReadLine();
                    byte[] buffer = Encoding.ASCII.GetBytes(texto);

                    cliente.GetStream().Write(buffer, 0, buffer.Length);
                }
            } catch { }
        }
    }
}
