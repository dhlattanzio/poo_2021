﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Cliente
{
    /*
    * Desarrollar un programa que utilizando socket permita escribir en una
    * aplicación que se esté ejecutando en una PC de la red y lo escrito se
    * pueda visualizar en la aplicación que está corriendo en otra PC de la red.
    */
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Conectando...");
                TcpClient cliente = new TcpClient();
                cliente.Connect("localhost", 5000);

                Console.WriteLine("Esperando mensajes...");

                while(true)
                {
                    byte[] buffer = new byte[1000];
                    int length = cliente.GetStream().Read(buffer, 0, buffer.Length);

                    string texto = Encoding.UTF8.GetString(buffer, 0, length);
                    Console.WriteLine($"Servidor: {texto}");
                }
            } catch { }
        }
    }
}
