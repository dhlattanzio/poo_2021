﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cliente
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nombre = textBox1.Text;
            if (string.IsNullOrWhiteSpace(nombre))
            {
                MessageBox.Show("El nombre no puede estar vacio.");
            } else
            {
                FormChat form = new FormChat(nombre);
                form.Location = Location;
                form.StartPosition = FormStartPosition.Manual;
                form.Show();
                Hide();
                form.FormClosing += delegate { this.Close(); };
            }
        }
    }
}
