﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cliente
{
    public partial class FormChat : Form
    {
        private string nombre;
        private TcpClient cliente;

        public FormChat(string nombre)
        {
            this.nombre = nombre;
            InitializeComponent();
        }

        private void FormChat_Load(object sender, EventArgs e)
        {
            try
            {
                cliente = new TcpClient();
                cliente.Connect("localhost", 5000);

                Thread thread = new Thread(EscucharMensajes);
                thread.Start();
            } catch(Exception ex)
            {
                Console.WriteLine($"Cliente Exception: {ex.Message}");
            }
        }

        private void EscucharMensajes()
        {
            try
            {
                while(true)
                {
                    byte[] buffer = new byte[1024];
                    int count = cliente.GetStream().Read(buffer, 0, buffer.Length);
                    string mensaje = Encoding.UTF8.GetString(buffer, 0, count);

                    //NuevoMensaje(mensaje);
                    this.Invoke(new Action(() => NuevoMensaje(mensaje)));
                }
            } catch(Exception e)
            {
                Console.WriteLine($"EscucharMensajes Exception: {e.Message}");
            }
        }

        private void EnviarMensaje(string mensaje)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(mensaje);
            cliente.GetStream().Write(buffer, 0, buffer.Length);
        }

        private void NuevoMensaje(string mensaje)
        {
            textBoxMensajes.Text = textBoxMensajes.Text + mensaje + "\r\n";
        }

        // Enviar
        private void button1_Click(object sender, EventArgs e)
        {
            string mensaje = textBox1.Text;
            if (!string.IsNullOrWhiteSpace(mensaje))
            {
                EnviarMensaje($"{nombre}: {mensaje}");
                textBox1.Text = "";
            }
        }
    }
}
