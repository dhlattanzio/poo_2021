﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Servidor
{
    class Servidor
    {
        private TcpListener servidor;
        private List<Cliente> clientes = new List<Cliente>();
        private List<Grupo> grupos = new List<Grupo>();

        private Grupo grupoPrincipal = new Grupo(null);

        public Servidor()
        {
            servidor = new TcpListener(System.Net.IPAddress.Any, 5000);
        }

        public void Iniciar()
        {
            try
            {
                servidor.Start();

                while(true)
                {
                    Console.WriteLine("Esperando nuevo cliente...");
                    TcpClient tcp = servidor.AcceptTcpClient();
                    Console.WriteLine("Nuevo cliente conectado!");

                    Cliente cliente = new Cliente(tcp);
                    cliente.Grupo = grupoPrincipal;
                    grupoPrincipal.AgregarCliente(cliente);
                    clientes.Add(cliente);

                    cliente.EventoNuevoMensaje += MensajeCliente;
                }
            } catch(Exception e)
            {
                Console.WriteLine($"Servidor Exception: {e.Message}");
            }
        }

        private void MensajeCliente(Cliente cliente, string mensaje)
        {
            cliente.Grupo.EnviarMensaje(cliente, mensaje);
            Console.WriteLine("Nuevo mensaje: " + mensaje);
        }
    }
}
