﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Servidor
{
    class Cliente
    {
        private TcpClient cliente;
        public string Nombre { get; set; }
        public Grupo Grupo { get; set; }

        public delegate void NuevoMensaje(Cliente cliente, string mensaje);
        public event NuevoMensaje EventoNuevoMensaje;

        public Cliente(TcpClient cliente)
        {
            this.cliente = cliente;
            Thread thread = new Thread(EscucharMensajes);
            thread.Start();
        }

        private void EscucharMensajes()
        {
            try
            {
                while(true)
                {
                    byte[] buffer = new byte[1024];
                    int count = cliente.GetStream().Read(buffer, 0, buffer.Length);
                    string mensaje = Encoding.UTF8.GetString(buffer, 0, count);

                    EventoNuevoMensaje?.Invoke(this, mensaje);
                }
            } catch(Exception e)
            {
                Console.WriteLine($"Cliente Exception: {e.Message}");
            }
        }

        public void EnviarMensaje(string mensaje)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(mensaje);
            cliente.GetStream().Write(buffer, 0, buffer.Length);
        }
    }
}
