﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servidor
{
    /*
     * Desarrollar un programa que utilizando socket que emule un servidor de chat.
     * También desarrollar el software cliente que se ejecutará en las Pc’s que se
     * conecten al servidor. Cada mensaje enviado por un cliente lo podrán recibir
     * todos los demás. En el caso que un cliente solicite comunicación privada
     * el mensaje se verá solo por los que participan de esta comunicación.
     * Un cliente puede solicitar armar un grupo y el resto unirse a él, para que
     * esto suceda el propietario del grupo deberá aceptarlos y tiene la potestad de
     * desconectarlos. El servidor es quien modera todo y por allí pasan todos los mensajes.
     */
    class Program
    {
        static void Main(string[] args)
        {
            Servidor servidor = new Servidor();
            servidor.Iniciar();
        }
    }
}
