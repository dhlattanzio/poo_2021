﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servidor
{
    class Grupo
    {
        public List<Cliente> Clientes { get; set; } = new List<Cliente>();

        private readonly Cliente creador;
        public Cliente Creador { get => creador; }

        public Grupo(Cliente creador)
        {
            this.creador = creador;
        }

        public void AgregarCliente(Cliente cliente)
        {
            Clientes.Add(cliente);
        }

        public void BorrarCliente(Cliente cliente)
        {
            Clientes.Remove(cliente);
        }

        public void EnviarMensaje(Cliente cliente, string mensaje)
        {
            foreach(Cliente c in Clientes)
            {
                c.EnviarMensaje(mensaje);
            }
        }
    }
}
