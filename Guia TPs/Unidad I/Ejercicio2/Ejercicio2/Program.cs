﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{

    /*
     * Defina una situación donde se desee llegar desde un estado inicia a uno final,
     * por ejemplo, una cuenta bancaria que posee un saldo inicial y se la somete a 
     * una operación bancaria. Desarrollar la clase cuenta con las propiedades y métodos
     * que considere pertinentes y demuestre como cambia el estado del objeto en la
     * medida que se le realizan depósitos, extracciones y transferencias.
     */
    class Program
    {
        static void Main(string[] args)
        {
            CuentaBancaria cuenta1 = new CuentaBancaria();
            CuentaBancaria cuenta2 = new CuentaBancaria();

            Console.WriteLine($"Cuenta 1 saldo: ${cuenta1.Saldo}");

            cuenta1.Depositar(1500);
            Console.WriteLine("Se deposito $1500 en la cuenta 1.");
            Console.WriteLine($"Cuenta 1 saldo: ${cuenta1.Saldo}");

            cuenta1.Extraer(600);
            Console.WriteLine("Se extrajo $600 de la cuenta 1.");
            Console.WriteLine($"Cuenta 1 saldo: ${cuenta1.Saldo}");

            Console.WriteLine($"Cuenta 2 saldo: ${cuenta2.Saldo}");
            cuenta1.Transferir(cuenta2, 500);
            Console.WriteLine("Se transfirio $500 de la cuenta 1 a la cuenta 2.");
            Console.WriteLine($"Cuenta 1 saldo: ${cuenta1.Saldo}");
            Console.WriteLine($"Cuenta 2 saldo: ${cuenta2.Saldo}");
            Console.ReadLine();
        }
    }
}
