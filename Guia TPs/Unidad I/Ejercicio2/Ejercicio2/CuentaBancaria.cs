﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class CuentaBancaria
    {
        private int _saldo = 0;
        public int Saldo { get => _saldo; }

        public void Depositar(int cantidad)
        {
            _saldo += cantidad;
        }

        public void Extraer(int cantidad)
        {
            _saldo -= cantidad;
        }

        public void Transferir(CuentaBancaria cuenta, int cantidad)
        {
            _saldo -= cantidad;
            cuenta.Depositar(cantidad);
        }
    }
}
