﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    class NotaDesaprobada : Nota
    {
        public NotaDesaprobada(int numero) : base(numero)
        {
        }

        // Devuelve verdadero si la nota se considera aprobada
        public override bool EstaAprobada()
        {
            return false;
        }
    }
}
