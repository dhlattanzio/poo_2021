﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    // Clase abstracta con estructura basica de una nota
    public abstract class Nota
    {
        // Valor de la nota
        protected int Numero { get; set; }

        public Nota(int numero)
        {
            Numero = numero;
        }

        // Devuelve verdadero si la nota se considera aprobada
        public abstract bool EstaAprobada();
    }
}
