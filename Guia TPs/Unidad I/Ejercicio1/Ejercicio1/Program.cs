﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            Examen examen = new Examen();
            examen.Nota = new NotaAprobada(7);
            Console.WriteLine($"Aprobada: {examen.Nota.EstaAprobada()}");
            Console.ReadLine();
        }
    }
}
