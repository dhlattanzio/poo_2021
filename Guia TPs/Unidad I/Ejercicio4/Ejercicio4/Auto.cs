﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio4
{
    class Auto : Vehiculo, Deportivo
    {
        public override int Velocidad()
        {
            return 180 + VelocidadExtra();
        }

        public int VelocidadExtra()
        {
            return 80;
        }
    }
}
