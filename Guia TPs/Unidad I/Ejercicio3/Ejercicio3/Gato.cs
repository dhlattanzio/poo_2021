﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio3
{
    class Gato : Animal
    {
        public override string HacerRuido()
        {
            return "Miau";
        }
    }
}
