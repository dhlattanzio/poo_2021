﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio3
{
    class Perro : Animal
    {
        public override string HacerRuido()
        {
            return "Guau";
        }
    }
}
