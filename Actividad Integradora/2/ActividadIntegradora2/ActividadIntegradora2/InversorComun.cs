﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadIntegradora2
{
    public class InversorComun : Inversor
    {
        public InversorComun(string nombre, string apellido, int dni) : base(nombre, apellido, dni)
        {
        }

        ~InversorComun()
        {
        }

        public override float CalcularComision(Accion accion, int cantidad)
        {
            float total = accion.Cotizacion * cantidad;
            return total * 0.01f;
        }
    }
}
