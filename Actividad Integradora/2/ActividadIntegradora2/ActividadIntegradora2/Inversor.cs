﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadIntegradora2
{
    public abstract class Inversor : Persona, IDisposable
    {
        public List<InversorAccion> Acciones { get; set; } = new List<InversorAccion>();
        
        public Inversor(string nombre, string apellido, int dni)
        {
            Nombre = nombre;
            Apellido = apellido;
            Dni = dni;
        }

        ~Inversor()
        {
        }

        public void AccionCambioCotizacion(Accion accion, float nuevaCotizacion)
        {
        }

        public abstract float CalcularComision(Accion accion, int cantidad);

        public void Dispose()
        {
            Acciones = null;
        }
    }
}
