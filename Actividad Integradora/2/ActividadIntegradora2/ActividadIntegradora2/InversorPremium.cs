﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadIntegradora2
{
    public class InversorPremium : Inversor
    {
        public InversorPremium(string nombre, string apellido, int dni) : base(nombre, apellido, dni)
        {
        }

        ~InversorPremium()
        {
        }

        public override float CalcularComision(Accion accion, int cantidad)
        {
            float total = accion.Cotizacion * cantidad;
            return total * ((total < 20000) ? 0.01f : 0.005f);
        }
    }
}
