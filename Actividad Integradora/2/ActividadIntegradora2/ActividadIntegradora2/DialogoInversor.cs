﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActividadIntegradora2
{
    public partial class DialogoInversor : Form
    {
        private Inversor inversor;
        public Inversor resultadoInversor;

        public DialogoInversor(Inversor inversor)
        {
            this.inversor = inversor;
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
        }

        private void DialogoInversor_Load(object sender, EventArgs e)
        {
            if (inversor != null)
            {
                textBox1.Text = inversor.Nombre;
                textBox2.Text = inversor.Apellido;
                textBox3.Text = inversor.Dni.ToString();
                textBox3.Enabled = false;

                checkBox1.Visible = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nombre = textBox1.Text;
            string apellido = textBox2.Text;
            string dni = textBox3.Text;

            if (string.IsNullOrWhiteSpace(nombre))
            {
                MessageBox.Show("El campo nombre es invalido.");
            }
            else if (string.IsNullOrWhiteSpace(apellido))
            {
                MessageBox.Show("El campo apellido es invalido.");
            }
            else if (string.IsNullOrWhiteSpace(dni))
            {
                MessageBox.Show("El campo dni es invalido.");
            } else
            {
                int dniNumero;
                bool ok = int.TryParse(dni, out dniNumero);
                if (ok)
                {
                    if (checkBox1.Checked)
                    {
                        resultadoInversor = new InversorPremium(nombre, apellido, dniNumero);
                    } else
                    {
                        resultadoInversor = new InversorComun(nombre, apellido, dniNumero);
                    }
                    
                    DialogResult = DialogResult.OK;
                    Close();
                } else
                {
                    MessageBox.Show("El campo dni solo puede tener numeros.");
                }
            }
        }
    }
}
