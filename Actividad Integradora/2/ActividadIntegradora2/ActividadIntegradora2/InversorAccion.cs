﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadIntegradora2
{
    public class InversorAccion
    {
        public Accion Accion { get; set; }
        public int Cantidad { get; set; }

        public InversorAccion(Accion accion, int cantidad)
        {
            Accion = accion;
            Cantidad = cantidad;
        }
    }
}
