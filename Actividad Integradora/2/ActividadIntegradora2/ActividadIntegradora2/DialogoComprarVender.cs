﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActividadIntegradora2
{
    public partial class DialogoComprarVender : Form
    {
        private Inversor inversor;
        private Accion accion;
        public int cantidad;

        public DialogoComprarVender(Inversor inversor, Accion accion)
        {
            this.inversor = inversor;
            this.accion = accion;
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string cantidad = textBox3.Text;
            if (string.IsNullOrWhiteSpace(cantidad))
            {
                MessageBox.Show("El campo de cantidad no puede estar vacio.");
            } else
            {
                int cantidadNumero;
                bool ok = int.TryParse(cantidad, out cantidadNumero);
                if (!ok)
                {
                    MessageBox.Show("El campo de cantidad no es un numero valido.");
                } else
                {
                    this.cantidad = cantidadNumero;
                    DialogResult = DialogResult.OK;
                    Close();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void DialogoComprarVender_Load(object sender, EventArgs e)
        {
            textBox1.Text = accion.Id;
            textBox2.Text = accion.Cotizacion.ToString();
            textBox1.Enabled = false;
            textBox2.Enabled = false;
        }
    }
}
