﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActividadIntegradora2
{
    public partial class DialogoAccion : Form
    {
        private Accion accion;
        public Accion resultadoAccion;

        public DialogoAccion(Accion accion)
        {
            this.accion = accion;
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
        }

        private void DialogoAccion_Load(object sender, EventArgs e)
        {
            if (accion != null)
            {
                textBox1.Text = accion.Id;
                textBox1.Enabled = false;

                textBox2.Text = accion.Denominacion;

                textBox3.Text = accion.Cotizacion.ToString();

                textBox4.Text = accion.CantidadEmitida.ToString();
                textBox4.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string id = textBox1.Text.ToUpper();
            string denominacion = textBox2.Text;
            string cotizacion = textBox3.Text;
            string cantidadEmitida = textBox4.Text;
            
            if (string.IsNullOrWhiteSpace(id))
            {
                MessageBox.Show("El campo id no puede estar vacio.");
            }
            else if (string.IsNullOrWhiteSpace(denominacion))
            {
                MessageBox.Show("El campo denominacion no puede estar vacio.");
            }
            else if (string.IsNullOrWhiteSpace(cotizacion))
            {
                MessageBox.Show("El campo cotizacion no puede estar vacio.");
            }
            else if (string.IsNullOrWhiteSpace(cantidadEmitida))
            {
                MessageBox.Show("El campo cantidad emitida no puede estar vacio.");
            } else
            {

                string[] partesId = id.Split('-');
                if (partesId.Length != 3 || partesId[0].Length != 4 || partesId[1].Length != 4
                    || partesId[2].Length != 4 || !partesId[0].All(Char.IsLetter)
                    || !partesId[1].All(Char.IsDigit)
                    || !(Char.IsLetter(partesId[2][0]) && Char.IsLetter(partesId[2][2])
                    && Char.IsDigit(partesId[2][1]) && Char.IsDigit(partesId[2][3])))
                {
                    MessageBox.Show("El formato del Id es invalido. Ejemplo: BGAL-4148-J4T3");
                    return;
                }

                float cotizacionNumero;
                bool ok = float.TryParse(cotizacion, out cotizacionNumero);
                if (!ok)
                {
                    MessageBox.Show("El campo cotizacion debe ser un numero valido.");
                } else
                {
                    int cantidadEmitidaNumero;
                    ok = int.TryParse(cantidadEmitida, out cantidadEmitidaNumero);
                    if (!ok)
                    {
                        MessageBox.Show("El campo cantidad emitida debe ser un numero valido.");
                    } else
                    {
                        DialogResult = DialogResult.OK;
                        if (accion != null)
                        {
                            accion.Denominacion = denominacion;
                            accion.Cotizacion = cotizacionNumero;
                            resultadoAccion = accion;
                            
                        } else
                        {
                            resultadoAccion = new Accion(id, denominacion, cotizacionNumero, cantidadEmitidaNumero);
                        }
                        Close();
                    }
                }
            }
        }
    }
}
