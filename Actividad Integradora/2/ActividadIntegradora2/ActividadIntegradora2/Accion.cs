﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadIntegradora2
{
    public class Accion : IDisposable, IEnumerable, IEnumerator
    {
        public string Id { get; set; }
        public string Denominacion { get; set; }
        public int CantidadEmitida { get; set; }
        public int CantidadDisponible { get; set; }

        private float cotizacion;

        public float Cotizacion {
            get { return cotizacion; }
            set {
                if (cotizacion != value)
                {
                    cotizacion = value;
                    EventoCambioCotizacion?.Invoke(this, value);
                }
                }
        }

        private int index = -1;
        public object Current => Id.Split('-')[Math.Max(0, index)];

        public delegate void CambioCotizacion(Accion accion, float nuevaCotizacion);
        public event CambioCotizacion EventoCambioCotizacion;

        public Accion(string id, string denominacion, float cotizacion, int cantidadEmitida)
        {
            Id = id;
            Denominacion = denominacion;
            Cotizacion = cotizacion;
            CantidadEmitida = CantidadDisponible = cantidadEmitida;
        }

        ~Accion()
        {
        }

        public void Dispose()
        {
        }

        public IEnumerator GetEnumerator()
        {
           return this;
        }

        public bool MoveNext()
        {
            if (index < Id.Split('-').Length - 1)
            {
                index++;
                return true;
            }
            return false;
        }

        public void Reset()
        {
            index = -1;
        }
    }
}
