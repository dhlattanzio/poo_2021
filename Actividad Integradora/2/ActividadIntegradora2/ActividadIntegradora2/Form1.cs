﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActividadIntegradora2
{
    public partial class Form1 : Form
    {
        private List<Inversor> listaInversores = new List<Inversor>();
        private List<Accion> listaAcciones = new List<Accion>();
        
        private float totalComunes = 0;
        private float totalPremiumMenos20k = 0;
        private float totalPremiumMas20k = 0;

        public Form1()
        {
            InitializeComponent();
            StartPosition = FormStartPosition.CenterScreen;
        }

        private void ActualizarListaInversores()
        {
            dataGridViewInversores.DataSource = null;
            dataGridViewInversores.DataSource = listaInversores;
        }

        private void ActualizarListaAcciones()
        {
            dataGridViewAcciones.DataSource = null;
            dataGridViewAcciones.DataSource = listaAcciones;
        }

        private void ActualizarAccionesInversor(Inversor inversor)
        {
            dataGridViewAccionesInversor.DataSource = null;
            dataGridViewAccionesInversor.DataSource = inversor.Acciones;
        }

        private void ActualizarTotales()
        {
            labelTotalComunes.Text = $"${totalComunes}";
            labelTotalPremiumMenos20k.Text = $"${totalPremiumMenos20k}";
            labelTotalPremiumMas20k.Text = $"${totalPremiumMas20k}";
            labelTotalGeneral.Text = $"${totalComunes + totalPremiumMenos20k + totalPremiumMas20k}";
        }

        // Agregar Inversor
        private void button1_Click(object sender, EventArgs e)
        {
            DialogoInversor dialogo = new DialogoInversor(null);
            DialogResult resultado = dialogo.ShowDialog();
            if (resultado == DialogResult.OK)
            {
                listaInversores.Add(dialogo.resultadoInversor);
                ActualizarListaInversores();
            }
        }

        // Borrar Inversor
        private void button2_Click(object sender, EventArgs e)
        {
            Inversor inversor = (Inversor)dataGridViewInversores.CurrentRow?.DataBoundItem;
            if (inversor != null)
            {
                listaInversores.Remove(inversor);
                ActualizarListaInversores();
            }
            else
            {
                MessageBox.Show("Selecciona un inversor de la lista");
            }
        }

        // Modificar Inversor
        private void button3_Click(object sender, EventArgs e)
        {
            Inversor inversor = (Inversor)dataGridViewInversores.CurrentRow?.DataBoundItem;
            if (inversor != null)
            {
                DialogoInversor dialogo = new DialogoInversor(inversor);
                DialogResult resultado = dialogo.ShowDialog();
                if (resultado == DialogResult.OK)
                {
                    listaInversores.Remove(inversor);
                    listaInversores.Add(dialogo.resultadoInversor);
                    ActualizarListaInversores();
                }
            }
            else
            {
                MessageBox.Show("Selecciona un inversor de la lista");
            }
        }

        // Agregar Accion
        private void button6_Click(object sender, EventArgs e)
        {
            DialogoAccion dialogo = new DialogoAccion(null);
            DialogResult resultado = dialogo.ShowDialog();
            if (resultado == DialogResult.OK)
            {
                listaAcciones.Add(dialogo.resultadoAccion);
                ActualizarListaAcciones();
            }
        }

        // Borrar Accion
        private void button5_Click(object sender, EventArgs e)
        {
            Accion accion = (Accion)dataGridViewAcciones.CurrentRow?.DataBoundItem;
            if (accion != null)
            {
                listaAcciones.Remove(accion);
                ActualizarListaAcciones();
            }
            else
            {
                MessageBox.Show("Selecciona una accion de la lista");
            }
        }

        // Modificar Accion
        private void button4_Click(object sender, EventArgs e)
        {
            Accion accion = (Accion)dataGridViewAcciones.CurrentRow?.DataBoundItem;
            if (accion != null)
            {
                DialogoAccion dialogo = new DialogoAccion(accion);
                DialogResult resultado = dialogo.ShowDialog();
                if (resultado == DialogResult.OK)
                {
                    listaAcciones.Remove(accion);
                    listaAcciones.Add(dialogo.resultadoAccion);
                    ActualizarListaAcciones();
                }
            }
            else
            {
                MessageBox.Show("Selecciona una accion de la lista");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void dataGridViewInversores_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Inversor inversor = (Inversor)dataGridViewInversores.CurrentRow?.DataBoundItem;
            if (inversor != null)
            {
                ActualizarAccionesInversor(inversor);
            }
        }

        // Comprar Accion
        private void button7_Click(object sender, EventArgs e)
        {
            Inversor inversor = (Inversor)dataGridViewInversores.CurrentRow?.DataBoundItem;
            Accion accion = (Accion)dataGridViewAcciones.CurrentRow?.DataBoundItem;
            if (inversor == null || accion == null)
            {
                MessageBox.Show("Selecciona un inversor y una accion.");
            } else
            {
                DialogoComprarVender dialogo = new DialogoComprarVender(inversor, accion);
                DialogResult resultado = dialogo.ShowDialog();
                if (resultado == DialogResult.OK)
                {
                    int cantidad = dialogo.cantidad;
                    if (cantidad > accion.CantidadDisponible)
                    {
                        MessageBox.Show("La accion indicada no tiene esa cantidad disponible para comprar.");
                    }
                    else
                    {
                        accion.CantidadDisponible -= cantidad;
                        InversorAccion invAct = inversor.Acciones.SingleOrDefault(x => x.Accion == accion);
                        if (invAct == null)
                        {
                            invAct = new InversorAccion(accion, 0);
                            accion.EventoCambioCotizacion += inversor.AccionCambioCotizacion;
                            inversor.Acciones.Add(invAct);
                        }
                        invAct.Cantidad += cantidad;

                        float comision = inversor.CalcularComision(accion, cantidad);
                        if (inversor is InversorComun)
                        {
                            totalComunes += comision;
                        } else
                        {
                            if (accion.Cotizacion * cantidad >= 20000)
                            {
                                totalPremiumMas20k += comision;
                            } else
                            {
                                totalPremiumMenos20k += comision;
                            }
                        }
                        ActualizarTotales();
                        ActualizarAccionesInversor(inversor);
                    }
                }
            }
        }

        // Vender Accion
        private void button8_Click(object sender, EventArgs e)
        {
            Inversor inversor = (Inversor)dataGridViewInversores.CurrentRow?.DataBoundItem;
            InversorAccion invAccion = (InversorAccion)dataGridViewAccionesInversor.CurrentRow?.DataBoundItem;
            if (inversor == null || invAccion == null)
            {
                MessageBox.Show("Selecciona un inversor y una accion.");
            }
            else
            {
                Accion accion = invAccion.Accion;
                DialogoComprarVender dialogo = new DialogoComprarVender(inversor, accion);
                DialogResult resultado = dialogo.ShowDialog();
                if (resultado == DialogResult.OK)
                {
                    int cantidad = dialogo.cantidad;
                    if (cantidad > inversor.Acciones.Single(x => x.Accion == accion).Cantidad)
                    {
                        MessageBox.Show("El inversor no tiene esa cantidad de acciones disponibles para vender.");
                    }
                    else
                    {


                        accion.CantidadDisponible += cantidad;
                        InversorAccion invAct = inversor.Acciones.Single(x => x.Accion == accion);
                        invAct.Cantidad -= cantidad;
                        if (invAct.Cantidad == 0)
                        {
                            inversor.Acciones.Remove(invAct);
                            invAct.Accion.EventoCambioCotizacion -= inversor.AccionCambioCotizacion;
                        }

                        float comision = inversor.CalcularComision(accion, cantidad);
                        if (inversor is InversorComun)
                        {
                            totalComunes += comision;
                        }
                        else
                        {
                            if (accion.Cotizacion * cantidad >= 20000)
                            {
                                totalPremiumMas20k += comision;
                            }
                            else
                            {
                                totalPremiumMenos20k += comision;
                            }
                        }
                        ActualizarTotales();
                        ActualizarAccionesInversor(inversor);
                    }
                }
            }
        }
    }
}
