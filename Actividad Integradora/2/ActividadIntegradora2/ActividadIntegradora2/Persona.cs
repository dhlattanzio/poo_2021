﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadIntegradora2
{
    public abstract class Persona : IDisposable
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Dni { get; set; }

        public Persona()
        {
        }

        ~Persona()
        {
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
