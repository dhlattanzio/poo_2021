﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadIntegradora1
{
    public class Persona
    {
        public string DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }

        private List<Auto> listaAutos = new List<Auto>();

        public Persona(string dni, string nombre, string apellido)
        {
            DNI = dni;
            Nombre = nombre;
            Apellido = apellido;
        }

        ~Persona()
        {
            Console.WriteLine($"Persona liberada. DNI: {DNI}");
        }

        public List<Auto> Lista_de_autos()
        {
            return listaAutos;
        }

        public int Cantidad_de_Autos()
        {
            return listaAutos.Count;
        }
    }
}
