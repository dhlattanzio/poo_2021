﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActividadIntegradora1
{
    public partial class Form1 : Form
    {
        private List<Persona> listaPersonas = new List<Persona>();
        private List<Auto> listaAutos = new List<Auto>();

        public Form1()
        {
            InitializeComponent();
        }

        private void ActualizarPersonas()
        {
            dataGridViewPersonas.DataSource = null;
            dataGridViewPersonas.DataSource = listaPersonas;
        }

        private void ActualizarAutos()
        {
            dataGridViewAutos.DataSource = null;
            dataGridViewAutos.DataSource = listaAutos;
            ActualizarGrillaAutosDetalle();
        }

        private void ActualizarGrillaAutosDetalle()
        {
            List<AutoDetalle> lista = listaAutos
                .Select(x => new AutoDetalle(x.Marca, x.Año, x.Modelo, x.Patente, x.Dueño())).ToList();

            dataGridViewDatosAutos.DataSource = null;
            dataGridViewDatosAutos.DataSource = lista;
        }

        private void CalcularTotal(Persona persona)
        {
            // TODO
            int total = listaAutos.Where(x => x.Dueño() == persona)
                .Aggregate(0, (acc, auto) => (int)(acc + auto.Precio));
            labelSumaPrecios.Text = "$" + total;
        }

        private void MostrarAutos(Persona persona)
        {
            List<Auto> lista = listaAutos.Where(x => x.Dueño() == persona).ToList();

            dataGridViewAutosPersona.DataSource = null;
            dataGridViewAutosPersona.DataSource = lista;
        }

        private void AsignarAuto(Persona persona, Auto auto)
        {
            auto.dueño = persona;

            CalcularTotal(persona);
            MostrarAutos(persona);
            ActualizarAutos();
        }

        // Persona - Agregar
        private void button1_Click(object sender, EventArgs e)
        {
            DialogoPersona dialogo = new DialogoPersona(null);
            DialogResult result = dialogo.ShowDialog();
            if (result == DialogResult.OK)
            {
                Persona persona = dialogo.resultadoPersona;
                listaPersonas.Add(persona);

                ActualizarPersonas();
            }
        }

        // Persona - Borrar
        private void button2_Click(object sender, EventArgs e)
        {
            Persona persona = (Persona)dataGridViewPersonas.CurrentRow?.DataBoundItem;
            listaPersonas.Remove(persona);
            ActualizarPersonas();
        }

        // Persona - Modificar
        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridViewPersonas.CurrentRow != null)
            {
                Persona personaActual = (Persona)dataGridViewPersonas.CurrentRow?.DataBoundItem;

                DialogoPersona dialogo = new DialogoPersona(personaActual);
                DialogResult result = dialogo.ShowDialog();
                if (result == DialogResult.OK)
                {
                    Persona persona = dialogo.resultadoPersona;
                    listaPersonas.Remove(personaActual);
                    listaPersonas.Add(persona);

                    ActualizarPersonas();
                }
            } else
            {
                MessageBox.Show("Selecciona una persona de la lista.");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        // Auto - Agregar
        private void button6_Click(object sender, EventArgs e)
        {
            DialogoAuto dialogo = new DialogoAuto(null);
            DialogResult result = dialogo.ShowDialog();
            if (result == DialogResult.OK)
            {
                Auto auto = dialogo.resultadoAuto;
                listaAutos.Add(auto);

                ActualizarAutos();
            }
        }

        // Auto - Borrar
        private void button5_Click(object sender, EventArgs e)
        {
            Auto auto = (Auto)dataGridViewAutos.CurrentRow?.DataBoundItem;
            listaAutos.Remove(auto);
            ActualizarAutos();
        }

        // Auto - Modificar
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridViewAutos.CurrentRow != null)
            {
                Auto autoActual = (Auto)dataGridViewAutos.CurrentRow?.DataBoundItem;

                DialogoAuto dialogo = new DialogoAuto(autoActual);
                DialogResult result = dialogo.ShowDialog();
                if (result == DialogResult.OK)
                {
                    Auto auto = dialogo.resultadoAuto;
                    listaAutos.Remove(autoActual);
                    listaAutos.Add(auto);

                    ActualizarAutos();
                }
            }
            else
            {
                MessageBox.Show("Selecciona un auto de la lista.");
            }
        }

        // Click persona
        private void dataGridViewPersonas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Persona persona = (Persona)dataGridViewPersonas.CurrentRow?.DataBoundItem;
            if (persona != null)
            {
                CalcularTotal(persona);
                MostrarAutos(persona);
            }
        }

        // Asignar auto
        private void button7_Click(object sender, EventArgs e)
        {
            Persona persona = (Persona)dataGridViewPersonas.CurrentRow?.DataBoundItem;
            Auto auto = (Auto)dataGridViewAutos.CurrentRow?.DataBoundItem;

            if (persona == null)
            {
                MessageBox.Show("Selecciona una persona de la lista.");
            }
            else if (auto == null)
            {
                MessageBox.Show("Selecciona un auto de la lista.");
            } else
            {
                AsignarAuto(persona, auto);
            }
        }
    }
}
