﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadIntegradora1
{
    class AutoDetalle
    {
        public string Marca { get; set; }
        public string Año { get; set; }
        public string Modelo { get; set; }
        public string Patente { get; set; }
        public string DniDueño { get; set; }
        public string ApellidoNombre { get; set; }

        public AutoDetalle(string marca, string año, string modelo, string patente, Persona dueño)
        {
            Marca = marca;
            Año = año;
            Modelo = modelo;
            Patente = patente;
            if (dueño != null)
            {
                DniDueño = dueño.DNI;
                ApellidoNombre = $"{dueño.Apellido}, {dueño.Nombre}";
            }
        }
    }
}
