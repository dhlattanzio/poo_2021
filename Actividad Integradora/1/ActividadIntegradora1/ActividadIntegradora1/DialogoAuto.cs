﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActividadIntegradora1
{
    public partial class DialogoAuto : Form
    {
        private Auto auto;
        public Auto resultadoAuto;

        public DialogoAuto(Auto auto)
        {
            this.auto = auto;
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string patente = textBox1.Text;
            string marca = textBox2.Text;
            string modelo = textBox3.Text;
            string año = textBox4.Text;
            string precio = textBox5.Text;

            if (string.IsNullOrWhiteSpace(patente))
            {
                MessageBox.Show("El campo patente no puede estar vacio.");
            }
            else if (string.IsNullOrWhiteSpace(marca))
            {
                MessageBox.Show("El campo marca no puede estar vacio.");
            }
            else if (string.IsNullOrWhiteSpace(modelo))
            {
                MessageBox.Show("El campo modelo no puede estar vacio.");
            }
            else if (string.IsNullOrWhiteSpace(año))
            {
                MessageBox.Show("El campo año no puede estar vacio.");
            }
            else if (string.IsNullOrWhiteSpace(precio))
            {
                MessageBox.Show("El campo precio no puede estar vacio.");
            } else
            {
                decimal precioNumero;
                bool ok = decimal.TryParse(precio, out precioNumero);
                if (ok)
                {
                    resultadoAuto = new Auto(patente, marca, modelo, año, precioNumero);

                    DialogResult = DialogResult.OK;
                    Close();
                } else
                {
                    MessageBox.Show("El campo precio debe ser un numero.");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void DialogoAuto_Load(object sender, EventArgs e)
        {
            if (auto != null)
            {
                textBox1.Text = auto.Patente;
                textBox2.Text = auto.Marca;
                textBox3.Text = auto.Modelo;
                textBox4.Text = auto.Año;
                textBox5.Text = auto.Precio.ToString();
            }
        }
    }
}
