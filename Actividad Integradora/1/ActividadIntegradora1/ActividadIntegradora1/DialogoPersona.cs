﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActividadIntegradora1
{
    public partial class DialogoPersona : Form
    {
        private Persona persona;
        public Persona resultadoPersona;

        public DialogoPersona(Persona persona)
        {
            this.persona = persona;
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
        }

        private void DialogoPersona_Load(object sender, EventArgs e)
        {
            if (persona != null)
            {
                textBox1.Text = persona.DNI;
                textBox2.Text = persona.Nombre;
                textBox3.Text = persona.Apellido;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string dni = textBox1.Text;
            string nombre = textBox2.Text;
            string apellido = textBox3.Text;

            if (string.IsNullOrWhiteSpace(dni))
            {
                MessageBox.Show("El campo DNI no puede estar vacio.");
            }
            else if (string.IsNullOrWhiteSpace(nombre))
            {
                MessageBox.Show("El campo nombre no puede estar vacio.");
            }
            else if (string.IsNullOrWhiteSpace(apellido))
            {
                MessageBox.Show("El campo apellido no puede estar vacio.");
            }
            else
            {
                int dniNumero;
                bool ok = int.TryParse(dni, out dniNumero);
                if (!ok)
                {
                    MessageBox.Show("El campo DNI no es valido. Solo puede tener numeros.");
                } else
                {
                    resultadoPersona = new Persona(dni, nombre, apellido);

                    DialogResult = DialogResult.OK;
                    Close();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
