﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActividadIntegradora3
{
    public partial class DialogoProducto : Form
    {
        private Producto producto;
        public Producto resultadoProducto;

        public DialogoProducto(Producto producto)
        {
            this.producto = producto;
            InitializeComponent();
            StartPosition = FormStartPosition.CenterParent;
        }

        private void DialogoProducto_Load(object sender, EventArgs e)
        {
            if (producto != null)
            {
                textBox1.Text = producto.Id;
                textBox2.Text = producto.Descripcion;
                textBox3.Text = producto.Precio.ToString();
                textBox4.Text = producto.Stock.ToString();
            }
        }

        private bool ValidarId(string id)
        {
            id = id.ToUpper();
            try
            {
                string[] partes = id.Split('-');

                if (partes[0].Length != 3) throw new Exception("El codigo de producto es invalido.");
                int codigoProducto = int.Parse(partes[0]);

                if (partes[1].Length != 3 || partes[1].ToCharArray()[0] != 'L')
                    throw new Exception("El numero de linea es invalido.");
                int linea = int.Parse(partes[1].Substring(1));

                if (partes[2].Length != 5 || !partes[2].StartsWith("OP"))
                    throw new Exception("El numero de linea es invalido.");
                int codigoOperador = int.Parse(partes[2].Substring(2));

                DateTime fecha = DateTime.Parse(partes[3]);
                return true;
            } catch(Exception e)
            {
                Console.WriteLine("Exception: "+e.Message);
                return false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string id = textBox1.Text;
            string descripcion = textBox2.Text;
            string precio = textBox3.Text;
            string stock = textBox4.Text;

            if (string.IsNullOrWhiteSpace(id))
            {
                MessageBox.Show("El campo id no puede estar vacio.");
            }
            else if (string.IsNullOrWhiteSpace(descripcion))
            {
                MessageBox.Show("El campo descripcion no puede estar vacio.");
            }
            else if (string.IsNullOrWhiteSpace(precio))
            {
                MessageBox.Show("El campo precio no puede estar vacio.");
            }
            else if (string.IsNullOrWhiteSpace(stock))
            {
                MessageBox.Show("El campo stock no puede estar vacio.");
            }
            else
            {
                if (!ValidarId(id))
                {
                    MessageBox.Show("El formato del id no es valido. Ejemplo: 001-L01-OP200-02/10/2018");
                } else
                {
                    decimal precioNumero;
                    bool ok = decimal.TryParse(precio, out precioNumero);
                    if (!ok)
                    {
                        MessageBox.Show("El precio debe ser un numero.");
                    } else
                    {
                        int stockNumero;
                        ok = int.TryParse(stock, out stockNumero);
                        if (!ok)
                        {
                            MessageBox.Show("El stock debe ser un numero.");
                        } else
                        {
                            if (producto != null)
                            {
                                producto.Id = id;
                                producto.Descripcion = descripcion;
                                producto.Precio = precioNumero;
                                producto.Stock = stockNumero;
                                resultadoProducto = producto;
                            } else
                            {
                                resultadoProducto = new Producto(id, descripcion, precioNumero, stockNumero);
                            }
                            
                            DialogResult = DialogResult.OK;
                            Close();
                        }
                    }

                    
                }
            }
        }
    }
}
