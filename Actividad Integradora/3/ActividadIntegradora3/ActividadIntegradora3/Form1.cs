﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActividadIntegradora3
{
    public partial class Form1 : Form
    {
        private List<Producto> listaProductos = new List<Producto>();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void ActualizarProductos()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = listaProductos;
        }

        // Agregar producto
        private void button1_Click(object sender, EventArgs e)
        {
            DialogoProducto dialogo = new DialogoProducto(null);
            DialogResult result = dialogo.ShowDialog();
            if (result == DialogResult.OK)
            {
                listaProductos.Add(dialogo.resultadoProducto);
                ActualizarProductos();
            }
        }

        // Borrar producto
        private void button2_Click(object sender, EventArgs e)
        {
            Producto producto = (Producto)dataGridView1.CurrentRow?.DataBoundItem;
            if (producto != null)
            {
                listaProductos.Remove(producto);
                ActualizarProductos();
            }
        }

        // Modificar producto
        private void button3_Click(object sender, EventArgs e)
        {
            Producto producto = (Producto)dataGridView1.CurrentRow?.DataBoundItem;
            if (producto != null)
            {
                DialogoProducto dialogo = new DialogoProducto(producto);
                DialogResult result = dialogo.ShowDialog();
                if (result == DialogResult.OK)
                {
                    listaProductos.Remove(producto);
                    listaProductos.Add(dialogo.resultadoProducto);
                    ActualizarProductos();
                }
            }
        }

        // Ordenar id asc
        private void button6_Click(object sender, EventArgs e)
        {
            listaProductos.Sort(new ComparerId());
            listaProductos.Reverse();
            ActualizarProductos();
        }

        // Ordenar id desc
        private void button5_Click(object sender, EventArgs e)
        {
            listaProductos.Sort(new ComparerId());
            ActualizarProductos();
        }

        // Ordenar precio asc
        private void button7_Click(object sender, EventArgs e)
        {
            listaProductos.Sort(new ComparerPrecio());
            listaProductos.Reverse();
            ActualizarProductos();
        }

        // Ordenar precio desc
        private void button4_Click(object sender, EventArgs e)
        {
            listaProductos.Sort(new ComparerPrecio());
            ActualizarProductos();
        }

        // Clonar
        private void button8_Click(object sender, EventArgs e)
        {
            Producto producto = (Producto)dataGridView1.CurrentRow?.DataBoundItem;
            if (producto != null)
            {
                listaProductos.Add((Producto)producto.Clone());
                ActualizarProductos();

                producto.Reset();
                foreach(string parte in producto)
                {
                    Console.WriteLine(parte);
                }
            } else
            {
                MessageBox.Show("Selecciona un producto de la lista para clonar.");
            }
        }
    }
}
