﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadIntegradora3
{
    public class Producto : ICloneable, IEnumerable, IEnumerator
    {
        public string Id { get; set; }
        public string Descripcion { get; set; }
        public decimal Precio { get; set; }
        public int Stock { get; set; }

        private int index = -1;
        public object Current => Id.Split('-')[Math.Max(index, 0)];

        public Producto(string id, string descripcion, decimal precio, int stock)
        {
            Id = id;
            Descripcion = descripcion;
            Precio = precio;
            Stock = stock;
        }

        public object Clone()
        {
            return new Producto(Id, Descripcion, Precio, Stock);
        }

        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }

        public bool MoveNext()
        {
            if (index < Id.Split('-').Length - 1)
            {
                index++;
                return true;
            }
            return false;
        }

        public void Reset()
        {
            index = -1;
        }
    }
}
