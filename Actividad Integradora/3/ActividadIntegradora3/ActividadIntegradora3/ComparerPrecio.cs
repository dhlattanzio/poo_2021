﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadIntegradora3
{
    public class ComparerPrecio : Comparer<Producto>
    {
        public override int Compare(Producto x, Producto y)
        {
            return (int)(x.Precio - y.Precio);
        }
    }
}
