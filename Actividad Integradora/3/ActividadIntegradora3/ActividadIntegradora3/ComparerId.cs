﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActividadIntegradora3
{
    public class ComparerId : Comparer<Producto>
    {
        public override int Compare(Producto x, Producto y)
        {
            return x.Id.CompareTo(y.Id);
        }
    }
}
